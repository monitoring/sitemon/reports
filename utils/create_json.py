import copy

from dateutil import parser


def createJson(src_data, legend):
    data = copy.deepcopy(src_data)
    for site in data.keys():
        data[site].pop('totals_global', None)
        data[site].pop('totals', None)
        for date in list(data[site]):
            parsed_date = parser.parse(date, dayfirst=False, yearfirst=True).strftime("%d-%m")
            data[site][parsed_date] = {}
            data[site][parsed_date]['CRIT'] = 100 - int(round(data[site][date][legend]))
            data[site][parsed_date]['OK'] = int(round(data[site][date][legend]))
            data[site][parsed_date]['SCHED'] = int(round(data[site][date]['reliability'])) - int(round(data[site][date]['availability']))
            data[site][parsed_date]['UNKNOWN'] = int(round(data[site][date]['unknown']))
            data[site].pop(date, None)
    return data


def createFedJson(src_data, summary_data):
    data = copy.deepcopy(src_data)
    month = max(data.keys())

    json_map = {}
    json_map = data[month]
    for fed in json_map.keys():
        json_map[fed]['Avl'] = int(round(json_map[fed]['availability']))
        json_map[fed].pop('availability')
        json_map[fed]['Rel'] = int(round(json_map[fed]['reliability']))
        json_map[fed].pop('reliability')
        for site in json_map[fed]['sites'].keys():
            json_map[fed]['sites'] = summary_data[fed]['sites']
            json_map[fed]['pledges'] = summary_data[fed]['pledges']

    return json_map
