
class TableUtils(object):

    @staticmethod
    def colorize(percent_float, dark):
        """
        Return color for table cell based on (int) percent
        """
        percent = int(round(percent_float))
        if not dark:
            if percent <= 0:
                color = '#FFFFFF;'
            elif percent < 30:
                color = '#FF5656;'
            elif percent < 60:
                color = '#FFAA00;'
            elif percent < 90:
                color = '#FBFF3E;'
            else:
                color = '#98E52F;'
        else:
            if percent <= 0:
                color = '#000000;'
            elif percent < 90:
                color = '#CC3300;'
            elif percent < 97:
                color = '#FF7722;'
            else:
                color = '#339900;'
        return color

    @staticmethod
    def decorstring(percent):
        """
        Return nice string for table cell based on (int) percent
        """
        if percent <= 0:
            percent = 0

        string = str(int(round(percent))) + '%'
        return string

    @staticmethod
    def build_color_coding_legend():
        """
        Builds the Color Coding legend table
        """
        return '''
                <table width="450px" border="1" bordercolor="white" style="padding-top:2px;margin-bottom:2px;padding-left:2px" align="center">
                        <tr>
                         <td width="30%" style="font-size:10pt;">Color coding:</td>
                         <td style="font-size:10pt;width:50px;">N/A</td>
                         <td style="font-size:10pt;width:50px;background:#FF5656"><30%</td>
                         <td style="font-size:10pt;width:50px;background:#FFAA00"><60%</td>
                         <td style="font-size:10pt;width:50px;background:#FBFF3E"><90%</td>
                         <td style="font-size:10pt;width:50px;background:#98E52F">>=90%</td>
                       </tr>
               </table>
            '''
