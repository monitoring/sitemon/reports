from dao import ETFDAO
from dateutil.relativedelta import relativedelta
from dateutil import parser


def getDates(data):
    aux = list(data)[0]
    return list(data[aux]['totals_global']) #[:-1]


def getRangedData_t0t1(es_user, es_password, es_max_buckets, dates, vo):
    t0t1_map = {}
    for month in dates:
        start_datetime = parser.parse(month).replace(day=1)
        end_datetime = start_datetime+relativedelta(months=+1)+relativedelta(days=-1)
        idb_month = ETFDAO(es_user, es_password, es_max_buckets, vo, start_datetime.strftime('%Y-%m-%dT00:00:00Z'), end_datetime.strftime('%Y-%m-%dT00:00:00Z'))
        all_tiers_month = idb_month.getDailySites(n_tiers=2)
        t0_t1_sites_month = all_tiers_month[0]
        t0_t1_sites_month.update(all_tiers_month[1])
        t0t1_map[month] = t0_t1_sites_month

    return t0t1_map


def getRangedData_t2(es_user, es_password, es_max_buckets, dates, vo):
    t2_map = {}
    for month in dates:
        start_datetime = parser.parse(month).replace(day=1)
        end_datetime = start_datetime+relativedelta(months=+1)+relativedelta(days=-1)
        idb_month = ETFDAO(es_user, es_password, es_max_buckets, vo, start_datetime.strftime('%Y-%m-%dT00:00:00Z'), end_datetime.strftime('%Y-%m-%dT00:00:00Z'))
        all_tiers_month = idb_month.getDailySites(n_tiers=3)
        t2_sites_month = all_tiers_month[2]
        t2_map[month] = t2_sites_month

    return t2_map
