import calendar

from pages.page import Page
from tables.federation_tables import *
from utils.table_utils import *
from collections import OrderedDict
import json

class FedHistPage(Page):
    def __init__(self, report, title, input_data, legend, only_one_month):
        subtitle = "Federation Details"
        Page.__init__(self, report, title, subtitle, input_data, legend, only_one_month)
        self.only_one_month = only_one_month

    def _createBody(self):
        html = TableUtils.build_color_coding_legend()
        html += PledgesTable(self._getHistMonths(self.stats), self._generateFederationSummary(self.stats)).buildPledgesTable()
        return html

    def _generateFederationSummary(self, input_data):
        current_month = sorted(input_data.keys(), reverse=True)[0]
        federations = sorted(input_data[current_month].keys())

        federation_summary = OrderedDict()
        for federation in federations:
            federation_summary[federation] = {}
            federation_summary[federation]['sites'] = {}
            sites = {}
            for site in input_data[current_month][federation]['sites']:
                availability = int(round(input_data[current_month][federation]['sites'][site]['availability']))
                reliability = int(round(input_data[current_month][federation]['sites'][site]['reliability']))
                unknown = int(round(input_data[current_month][federation]['sites'][site]['unknown']))
                if self.only_one_month:
                    sites[site] = {'Avl': availability, 'Rel': reliability, 'Unk': unknown}
                else:
                    sites[site] = {'Avl': availability, 'Rel': reliability, 'Unk': unknown, 'Avl1': 0, 'Avl2': 0, 'Avl3': 0}


                    for i in range(1, 4):
                        if federation in input_data[list(input_data.items())[i-1][0]]:
                            if site in input_data[list(input_data.items())[i-1][0]][federation]['sites']:
                                sites[site]['Avl'+str(i)] = int(round(input_data[list(input_data.items())[i-1][0]][federation]['sites'][site]['availability']))

            federation_summary[federation]['sites'] = sites
            federation_summary[federation]['pledges'] = input_data[current_month][federation]['pledges']

        return federation_summary

    def _getHistMonths(self, input_data):
        result = []
        for m in input_data:
            date = str.split(m, '-')
            month = int(date[1])
            year = date[0]
            new_date = calendar.month_abbr[month] + '-' + year
            result.append(new_date)
        return result
