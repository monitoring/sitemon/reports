import datetime
import collections
import os
import urllib, urllib3
import string
import random
import plotly.graph_objects as go

from .page import Page
from dateutil import parser
from tempfile import NamedTemporaryFile


class T0T1Page(Page):
    plots_per_row = 4

    def __init__(self, report, input_data, title, legend, start_time=None, end_time=None):
        subtitle = ' Target %s for each site is 97.0%%. Target for 8 best sites is 98.0%% ' % (legend)
        Page.__init__(self, report, title, subtitle, input_data, legend, start_time=start_time, end_time=end_time)

    def _createBody(self):
        """
        Return html string with plots inside
        Availability'/'Reliability'
        """
        if self.legend == 'Availability':
            color = '#77DD76'
            displayName = 'Avail'
        else:
            color = '#B4CBF0'
            displayName = "Rel"

        html = '''
            <table align="center">
                <tr>
                <td>
                    <table cellpadding="1">
                <tr>'''
        plots_description = ''

        sites = list(self.stats.keys())
        sites.sort()
        for index, site in enumerate(sites):
            plot_html, plot_description = self._createFigure(site, displayName, color)
            html += plot_html
            plots_description += plot_description

            # If max plots per row or last plot
            if ((index + 1)%self.plots_per_row == 0) or (len(self.stats) - 1 == index):
                html += '</tr><tr style="font-size: 0.8em;font-name=Helvetica-Bold;">'
                html += plots_description

                # If not the last plot
                if len(self.stats) - 1 != index:
                    html += '<tr>'

                plots_description = ''

        html += '''
                </tr>
              </table>
            </td>
          </tr>
        </table>'''

        return html


    def _createFigure(self, site, display_name, color):
        results = self.stats[site]
        hostname = self.report.request

        siteUp = []
        siteDown = []
        siteMaintenance = []
        siteUnknown = []

        plot_html = ''

        for index in results:
            if index != 'totals' and index != 'totals_global':
                siteUp.append(results[index][self.legend.lower()])
                siteDown.append((100 - results[index]['reliability']) - results[index]['unknown'])
                if self.legend == 'Availability':
                    siteMaintenance.append(results[index]['reliability'] - results[index]['availability'])
                siteUnknown.append(results[index]['unknown'])

        def round_number(number):
            return round(number, 0)

        plot_description = '''
               <td class="center">
                   <b>''' + str(site) + '''</b>
               </td>
               <td class="center">
                   <b>''' + display_name + ''': ''' + str(round_number(results['totals'][self.legend.lower()])) + '''%</b>
               </td>
               <td class="center">
                   <b>Unkn: ''' + str(round_number(results['totals']['unknown'])) + '''%</b>
               </td>'''

        x_axis = self._plotTimeline()
        fig = go.Figure(data=[
          go.Bar(name='Up', marker_color=color, x=x_axis, y=list(map(round_number, siteUp))),
          go.Bar(name='Down', marker_color='#FF6962', x=x_axis, y=list(map(round_number, siteDown))),
          go.Bar(name='Maintenance', marker_color='#FFB340', x=x_axis, y=list(map(round_number, siteMaintenance))),
          go.Bar(name='Unknown', marker_color='lightgray', x=x_axis, y=list(map(round_number, siteUnknown)))
        ])

        tick_number = len(x_axis)/3
        if (len(x_axis) > 31):
          months = len(x_axis)/30
          tick_number = len(x_axis)/months

        self._updateFigureLayout(site, fig, tick_number)

        # For the time being we write the plots locally in a folder
        if not os.path.exists('./plots'):
            os.makedirs('./plots')

        file_name = "./plots/sitemon." + ''.join(random.choices(string.ascii_uppercase, k=8)) + '.png'
        fig.write_image(file_name)
        plot_html += f'<td colspan=3 class="colplot"><img width=300 height=200 src="{file_name}" /></td>'

        return plot_html, plot_description

    def _updateFigureLayout(self, site, fig, tick_size):
      start_time = self.start_datetime.strftime("%d-%b-%Y")
      end_time = (self.end_datetime - datetime.timedelta(days=1)).strftime("%d-%b-%Y")
      fig.update_layout(
        title = {
          'text': f"Site:{site} <br>{self.legend} from {start_time} to {end_time}",
          'x': 0.5,
          'xanchor': 'center',
          'yanchor': 'top'},
        autosize = False,
        width = 600,
        height = 400,
        legend = {
          'x': 0.5,
          'xanchor': 'center',
          'y': -0.1,
          'yanchor': 'top',
          'orientation': 'h',
          'traceorder': 'normal'
        },
        barmode = 'stack',
        xaxis = {
          'type': 'category',
          'tickmode': 'linear',
          'dtick': tick_size,
        },
        yaxis = {
          'ticksuffix': '%'
        },
        font = {
          'size': 18
        }
      )

    def _plotTimeline(self):
        start_time = self.start_datetime
        end_time = self.end_datetime
        plot_x = []

        while start_time < end_time:
            plot_x.append(start_time.strftime("%d-%m"))
            start_time += datetime.timedelta(days = 1)

        return plot_x
