import datetime
import os
import time
import urllib

from dateutil import parser
from tempfile import NamedTemporaryFile


class Page():
    time_format = '"%Y-%m-%dT%H:%M:%SZ"'
    up = 'OK'
    down = 'CRITICAL'
    maintenance = 'DOWNTIME'
    unknown = 'UNKNOWN'

    def __init__(self, report, title, subtitle, input_data, legend, only_one_month=False, start_time=None, end_time=None):
        self.report = report
        self.title = title
        self.subtitle = subtitle
        self.legend = legend
        self.input_data = input_data
        self.only_one_month = only_one_month

        if start_time is not None:
            self.start_datetime = parser.parse(start_time)
        else:
            self.start_datetime = parser.parse(report.start_time)

        if end_time is not None:
            self.end_datetime = parser.parse(end_time)
        else:
            self.end_datetime = parser.parse(report.end_time)

        # Final objects to serve
        self.stats = input_data
        
        if not only_one_month:
            self.html = self._createPage()

    def _createPage(self):
        html = '<div class="report new-page" width="100%">\n'
        html += self._createHeader()
        html += self._createBody()
#        html += '</div><pdf:nextpage/>\n'
        html += '</div>\n'

        return html

    def _createHeader(self):
        logo_path = 'www/images/WLCG-logo.png'

        html = '''
        <!--  THIS PART IS THE HEADER  OF THE PAGE -->
            <table width=100% style="font-size:1.2em;">
                <tr>
                    <td rowspan=4 valign=top width=10%>
                        <img src="''' + logo_path + '''" />
                    </td>
                    <td align=center>
                        <b>''' + self.title + '''</b>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align=center><b>''' + self.report.vo.upper() + '''</b>
                    </td>
                    <td align=center valign=top width=15% rowspan=3><font color="blue"><b>''' + self.report.month + '''</b></font>
                    </td>
                </tr>
                <tr>
                    <td align=center><font color="blue">''' + self.subtitle + '''</font>
                    </td>
                </tr>
                <tr>
                    <td align=center style="font-size: 0.8em;">Availability Algorithm: ''' + self.report.profile + '''
                    </td>
                </tr>
            </table>
        <!-- END OF THE HEADER -->
        '''

        return html
