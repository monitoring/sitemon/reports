from pages.page import Page
from tables.vo_tables import VOTable


class VOPage(Page):
    def __init__(self, report, input_data, legend):
        title = 'Site ' + legend + ' of WLCG Tier-0 + Tier-1 Sites'
        subtitle = '''
                Target ''' + legend + ''' for each site is 97.0%.
                <div>
                    <span style="color:black">Colors: </span>
                    <span style="color:#CC3300;">Red <90%</span>
                    <span style="color:#FF9933;">Orange <97%</span>
                    <span style="color:#33AA00;">Green >= 97%</span>
                </div>
             '''
        Page.__init__(self, report, title, subtitle, input_data, legend)

    def _createBody(self):
        if self.legend == 'Availability':
            state = 'Avl'
        else:
            state = 'Rel'

        html = VOTable(self.input_data, self.legend, state).buildTable()
        return html
