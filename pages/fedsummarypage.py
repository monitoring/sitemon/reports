from pages.page import Page
from tables.federation_tables import *
from utils.table_utils import *


class FedSummaryByNamePage(Page):
    def __init__(self, report, title, input_data, legend):
        subtitle = "Federation Summary - Sorted by Name"
        Page.__init__(self, report, title, subtitle, input_data, legend)

    def _createBody(self):
        html = TableUtils.build_color_coding_legend()
        html += SummaryTable(self._generateStats(self.stats), 'Name').buildSummaryTable()
        return html

    def _generateStats(self, input_data):
        all_month_federations = list(input_data.items())
        last_month_federations = all_month_federations[-1][1]
        return last_month_federations


class FedSummaryByAvailabilityPage(Page):
    def __init__(self, report, title, input_data, legend):
        subtitle = "Federation Summary - Sorted by Availability"
        Page.__init__(self, report, title, subtitle, input_data, legend)

    def _createBody(self):
        html = TableUtils.build_color_coding_legend()
        html += SummaryTable(self._generateStats(self.stats), 'Availability').buildSummaryTable()
        return html

    def _generateStats(self, input_data):
        all_month_federations = list(input_data.items())
        last_month_federations = all_month_federations[-1][1]
        return last_month_federations
