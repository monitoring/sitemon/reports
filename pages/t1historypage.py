from .page import Page
from tables.history_tables import HistoryTable
from tables.detailed_history_tables import HistoryDetailedTable


class T1HistoryPage(Page):
    def __init__(self, report, input_data):
        title = 'WLCG Sites Reliability'
        subtitle = 'From ' + list(input_data.keys())[-1:][0] + '''<br/>Target for each site is 97.0% <br/>
    </font> Colors: <font color="green"> Green &gt; Target </font>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="darkorange">Orange &gt; 90% of Target</font>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red" >Red &lt; 90% of Target </font><font>'''
        Page.__init__(self, report, title, subtitle, input_data, '')

    def _createBody(self):
        html = HistoryTable(self.input_data).buildHistoryTables()
        html += '<br>'
        html += HistoryDetailedTable(self.input_data).buildDetailedHistoryTable()
        return html
