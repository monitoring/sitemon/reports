import datetime
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
import urllib
import copy

from dateutil import parser, relativedelta
from collections import OrderedDict
from functools import reduce
from es import ElasticSource


class ETFDAO():

    def __init__(self, es_user, es_password, es_max_buckets, vo, start_time, end_time):
        self.es = ElasticSource(es_user, es_password, es_max_buckets, vo, start_time, end_time)
        self.vo = vo.lower()
        self.start_time = start_time
        self.end_time = end_time
        self.authorization_header = {'Authorization': 'Bearer eyJrIjoiS3B0MjVsclo1WkRuYjlwZndOVUEwTlZKU1g2c2laNDciLCJuIjoic2FtX3JlcG9ydCIsImlkIjoyMH0='}

    def _getPledgesDataByTierAndTimeWindow(self, tier, start_time, end_time):
        payload = {
            'q': "SELECT last(value) FROM pledges WHERE vo = '%s' AND time >= '%s' AND time < '%s' AND tier = 'Tier %s' GROUP BY \"pledge_type\", \"federation\"" % (
                self.vo, start_time, end_time, tier
            )
        }

        r = requests.get('https://monit-grafana.cern.ch/api/datasources/proxy/9575/query?db=monit_production_cric',
                         params=urllib.parse.urlencode(payload),
                         headers=self.authorization_header)

        return r.json()['results'][0]['series']

    def getDailySites(self, n_tiers):
        self.start_datetime = parser.parse(self.start_time)
        self.end_datetime = parser.parse(self.end_time)
        month = day = self.start_datetime.strftime('%Y-%m')

        tiers = {}
        for tier in range(n_tiers):
            tiers[tier] = {}
            results_daily = self.es.getDataByTier(tier, "day", self.start_time, self.end_time, True)
            results_monthly = self.es.getDataByTier(tier, "month", self.start_time, self.end_time, True)
            for serie in results_daily:
                site = results_daily[serie]['site']
                daily_stats = OrderedDict(results_daily[serie]['date'])
                monthly_stats = OrderedDict(results_monthly[serie]['date'])

                tiers[tier][site] = daily_stats
                tiers[tier][site]['totals'] = self.calcAvgTotals(self._getMonthly(monthly_stats))
                tiers[tier][site]['totals_global'] = self._getMonthly(monthly_stats)

        return tiers

    def calcAvgTotals(self,totals):
        availability = 0
        reliability = 0
        unknown = 0
        num_bins = len(totals)

        for total in totals.values():
            availability += total['availability']
            reliability += total['reliability']
            unknown += total['unknown']

        return {'availability': availability/num_bins if availability > 0 else 0,
                'reliability': reliability/num_bins if reliability > 0 else 0,
                'unknown': unknown/num_bins if unknown > 0 else 0}

    def getT0T1MonthlyReliability(self):
        data = self.getDailySites(n_tiers=2)
        sites = list(data[0].keys()) + list(data[1].keys())
        aux = list(data[0].keys())[0]
        dates = list(data[0][aux]['totals_global'].keys())
        totals = {}

        current_month = dates[-1]

        for month in dates:
            totals[month] = {}
            for s in sites:
                if s in data[0] and current_month in data[0][s]['totals_global'] and month in data[0][s]['totals_global']:
                    totals[month][s] = data[0][s]['totals_global'][month]['reliability']
                elif s in data[1] and current_month in data[1][s]['totals_global'] and month in data[1][s]['totals_global']: # In case the site has dissapear we don't add it in the current report
                    totals[month][s] = data[1][s]['totals_global'][month]['reliability']

        return totals

    def getHistoricalFederationsFor(self, past_months=1, tier=2):
        self.start_datetime = (parser.parse(self.start_time) - relativedelta.relativedelta(months=past_months))
        self.end_datetime = parser.parse(self.end_time)

        federations = {}
        start_time = self.start_datetime.strftime('%Y-%m-%dT%H:%M:%SZ')
        results = self.es.getDataByTier(tier, "month", start_time, self.end_time)
        for serie in results:
            federation = results[serie]['federation']
            site = results[serie]['site']
            daily_stats = OrderedDict(results[serie]['date'])
            if federation != 'UNKNOWN':
                if federation not in federations:
                    federations[federation] = {}

                federations[federation][site] = self._getMonthly(daily_stats)

        return self._getSummaryPerFederation(federations)

    def getHistoricalFederationsWithPledgesFor(self, past_months=1, tier=2):
        federations_data = self.getHistoricalFederationsFor(past_months, tier)
        pledges_data = self.getFederationPledgesByTier(tier)
        summary = OrderedDict()

        for month in federations_data.keys():
            summary[month] = OrderedDict()
            for federation in federations_data[month].keys():
                federation_data = federations_data[month][federation]
                federation_data['pledges'] = {}
                if federation in pledges_data:
                    for pledge in pledges_data[federation].keys():
                        federation_data['pledges'][pledge] = pledges_data[federation][pledge]
                else:
                    default_pledges = {'CPU': -1, 'DISK': -1}
                    federation_data['pledges'] = default_pledges
                summary[month][federation] = federation_data

        return summary

    def getFederationPledgesByTier(self, tier):
        federation_pledges = {}
        tier_data = self._getPledgesDataByTierAndTimeWindow(tier, self.start_time, self.end_time)
        for serie in tier_data:
            federation = serie['tags']['federation']
            pledge_type = str.upper(str(serie['tags']['pledge_type']))
            value = serie['values'][0][1]
            if federation not in federation_pledges:
                federation_pledges[federation] = dict()

            federation_pledges[federation][pledge_type] = value

        return federation_pledges

    def _getSummaryPerFederation(self, federations):
        summary = OrderedDict()

        for federation in federations:
            if federation != 'UNKNOWN':
                for site in federations[federation]:
                    for month in federations[federation][site]:
                        if month not in summary:
                            summary[month] = OrderedDict()
                        if federation not in summary[month]:
                            summary[month][federation] = {}
                            summary[month][federation]['sites'] = {}
                        if site not in summary[month][federation]:
                            summary[month][federation]['sites'][site] = {}

                        summary[month][federation]['sites'][site].update(
                            federations[federation][site][month])

                for month in summary:
                    if federation in summary[month]:
                        summary[month][federation]['availability'] = self._computeMonthlyStatsPerFederation(summary[month][federation]['sites'], 'availability')
                        summary[month][federation]['reliability'] = self._computeMonthlyStatsPerFederation(summary[month][federation]['sites'], 'reliability')
                        summary[month][federation]['unknown'] = self._computeMonthlyStatsPerFederation(summary[month][federation]['sites'], 'unknown')

        return summary

    def _computeMonthlyStatsPerFederation(self, sites, computation):
        stat = 0
        for site in sites:
            stat += sites[site][computation]

        return stat / len(sites)

    def _getMonthly(self, stats):
        monthly_dates = OrderedDict()
        monthly_stats = copy.deepcopy(stats)
        for month in monthly_stats:
            if month not in monthly_dates:
                monthly_dates[month] = {}
                monthly_dates[month]['availability'] = []
                monthly_dates[month]['reliability'] = []
                monthly_dates[month]['unknown'] = []

            monthly_dates[month]['availability'] = monthly_stats[month]['availability']
            monthly_dates[month]['reliability'] = monthly_stats[month]['reliability']
            monthly_dates[month]['unknown'] = monthly_stats[month]['unknown']

            # If the full month is unkown we don't use it for availability (historical data)
            if month < '2020-07':
                if monthly_dates[month]['availability'] - monthly_dates[month]['unknown'] <= 0:
                    monthly_dates[month]['availability'] = 0
                if monthly_dates[month]['reliability'] - monthly_dates[month]['unknown'] <= 0:
                    monthly_dates[month]['reliability'] = 0

        return monthly_dates
