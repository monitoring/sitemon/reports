from utils.table_utils import TableUtils
from dateutil import parser


class HistoryDetailedTable:
    def __init__(self, hist_dates):
        self.hist_dates = hist_dates

    def buildDetailedHistoryTable(self):
        table_style = "text-align: center;border: 1px solid #aaaaaa;width:100%; width:90%;border-collapse: collapse;\
        border:1pt solid black;padding-bottom:2px;padding-top:5px;padding-right:1px;padding-left:1px;"

        dates = sorted(self.hist_dates.keys())
        sites = self.hist_dates[dates[0]].keys()
        html_aux = ''
        if not sites:
            html_aux = '''
                     <tr>
                        <td>N/A</td>
                     </tr>
                    '''
        else:
            for month in dates:
                html_aux += '<td class="nobreak" style="width:12%;background-color:palegoldenrod;font-weight:bold" ">' + \
                    parser.parse(month).strftime('%b-%Y') + '</td>'

            html_aux += '</tr>'

            for i in sites:
                html_aux += '<tr><td class="nobreak" style="width:28%;background-color:palegoldenrod;font-weight:bold">' + i + '</td>'
                for month in dates:
                    if i in self.hist_dates[month]:
                        value = int(self.hist_dates[month][i] + 0.5)
                        html_aux += '<td style="color:' + \
                            TableUtils.colorize(value, 1) + '">' + TableUtils.decorstring(value) + '</td>'
                html_aux += '</tr>\n\n'

            html_aux += '<tr><td >Target</td>'
            for i in dates:
                html_aux += '<td style="width:12%;">97%</td>'
            html_aux += '</tr>\n'
        html = '''
                <table style="font-size:10pt;" align="center">
                <tr>
                <td valign="top" style="width:100%;text-align:center;">
                Detailed Monthly Site Reliability
                <table class="tableWithBorders" style="''' + table_style + '''">
                    <tr> <td style="width:28%;background-color:palegoldenrod;font-weight:bold">Site </td>
                '''
        html += html_aux
        html += '''
                </table>
                </td>
                </tr>
                </table>
                '''
        return html
