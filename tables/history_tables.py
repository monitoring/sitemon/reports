from utils.table_utils import TableUtils
from dateutil import parser


class HistoryTable:
    def __init__(self, hist_dates):
        self.hist_dates = hist_dates

    def buildHistoryTables(self):
        table_style = "text-align: center;border: 1px solid #aaaaaa;width:100%; width:90%;border-collapse: collapse;\
        border:1pt solid black;padding-bottom:2px;padding-top:5px;padding-right:1px;padding-left:1px;"
        html1, html2 = self._buildHistoryContent()
        html = '''
                <table style="font-size:10pt;padding-bottom:15px;padding-right:45px;padding-left:45px;" align="center">
                <tr>
               '''
        html += self._buildAverageBestTable(table_style, html1)
        html += self._buildAverageAllTable(table_style, html2)
        html += '''
                </tr>
                </table>
                '''
        return html

    def _buildAverageBestTable(self, table_style, content):
        html = '''
                <td valign="top" style="width:50%;text-align:center;">
                Average of 8 best sites<br>
                (not always the same sites)
                <table class="tableWithBorders" id="avlTable" style="''' + table_style + '''">
                    <tr><td style="background-color:palegoldenrod;font-weight:bold" >Month </td> <td style="background-color:palegoldenrod;font-weight:bold" > Reliability</td></tr>
                '''
        html += content
        html += '''
                </table>
                </td>
                '''
        return html

    def _buildAverageAllTable(self, table_style, content):
        html = '''
                <td valign="top" style="width:50%;text-align:center;">
                Average of ALL Tier-0 and<br>Tier-1 sites
                <table class="tableWithBorders" id="relTable" style="''' + table_style + '''">
                    <tr><td style="background-color:palegoldenrod;font-weight:bold"> Month  </td> <td style="background-color:palegoldenrod;font-weight:bold">   Reliability  </td>   </tr>
                '''
        html += content
        html += '''
                </table>
                </td>
                '''
        return html

    def _buildHistoryContent(self):
        html1 = ''
        html2 = ''
        dates = sorted(self.hist_dates.keys())
        sites = self.hist_dates[dates[0]].keys()

        if not sites:
            html1 = '''
                     <tr>
                        <td>N/A</td>
                        <td>N/A</td>
                     </tr>
                    '''
            html2 = html1
        else:
            best_avg = {}
            total_avg = {}
            for month in dates:
                sorted_reliability = sorted(self.hist_dates[month].values())
                best_avg[month] = sum(sorted_reliability[-8:]) / 8
                total_avg[month] = sum(sorted_reliability) / len(sorted_reliability)

                html1 += '<tr><td class="nobreak">' + parser.parse(month).strftime('%b-%Y') + '</td>'
                html2 += '<tr><td class="nobreak">' + parser.parse(month).strftime('%b-%Y') + '</td>'

                value1 = int(best_avg[month] + 0.5)
                value2 = int(total_avg[month] + 0.5)

                html1 += '''
                            <td class="nobreak" style="color:''' + TableUtils.colorize(value1, 1) + '''">''' + TableUtils.decorstring(value1) + '''</td>
                            </tr>
                         '''

                html2 += '''
                            <td class="nobreak" style="color:''' + TableUtils.colorize(value2, 1) + '''">''' + TableUtils.decorstring(value2) + '''</td>
                            </tr>
                         '''
        return html1, html2
