from utils.table_utils import TableUtils


class PledgesTable:
    def __init__(self, hist_dates, federations_stats):
        self.hist_dates = hist_dates
        self.federations_stats = federations_stats

    def buildPledgesTable(self):
        html = self._buildPledgesTableHeader()
        html += self._buildPledgesTableContent()
        return html

    def _buildPledgesTableHeader(self):
        html = '''
                <table align="left" border="1" bordercolor="white"  style="padding-left:2px;padding-right:2px;padding-top:2px;" >
                    <tr style="font-weight:bold;text-align:center">
                        <td colspan="7">
                        </td>
                        <td colspan="3">
                        Availability History
                        </td>
                    </tr>
                    <tr style="font-weight:bold">
                        <td style="width:28%;border-bottom:1pt solid black;">
                        Federation & Sites
                        </td>
                        <td style="width:8%;border-bottom:1pt solid black;">
                        Pledge CPU
                        </td>
                        <td style="width:8%;border-bottom:1pt solid black;">
                        Pledge Disk
                        </td>
                        <td style="width:8%;border-bottom:1pt solid black;">
                        Availability
                        </td>
                        <td style="width:8%;border-bottom:1pt solid black;">
                        Reliability
                        </td>
                        <td style="width:8%;border-bottom:1pt solid black;">
                        Unknown
                        </td>
                        <td style="width:8%;border-bottom:1pt solid black;">
                        ''' + self.hist_dates[0] + '''
                        </td>
                        <td style="width:8%;border-bottom:1pt solid black;">
                        ''' + self.hist_dates[1] + '''
                        </td>
                        <td style="width:8%;border-bottom:1pt solid black;">
                        ''' + self.hist_dates[2] + '''
                        </td>
                    </tr>
                '''
        return html

    def _buildPledgesTableContent(self):
        html = ''
        all_federations = list(self.federations_stats.keys())
        all_federations.sort()

        for fed in all_federations:
            try:
                # Some federations may lack one of the two numbers
                cpu = self.federations_stats[fed]['pledges']['CPU'] if 'CPU' in self.federations_stats[fed]['pledges'] else -1
                disk = self.federations_stats[fed]['pledges']['DISK'] if 'DISK' in self.federations_stats[fed]['pledges'] else -1
                html += self._buildFederationPledges(
                    fed, cpu, disk)
                all_sites = list(self.federations_stats[fed]['sites'].keys())

                all_sites.sort()
                for site in all_sites:
                    entry = self.federations_stats[fed]['sites'][str(site)]
                    html += self._buildFederationSites(site, entry)
            except Exception as e:
                print("Federations Table: %s, %s" % (fed, str(e)))

        html += '''
                </table>
                '''
        return html

    def _buildFederationPledges(self, federation_name, pledge_cpu, pledge_disk):
        return '''<tr><td colspan="10"><hr/></td></tr><tr>
                        <td  style="border-bottom:1pt;solid black;font-weight:bold">''' + federation_name + '''</td>
                        <td  style="border-bottom:1pt;solid black;font-weight:bold">''' + str(pledge_cpu) + '''</td>
                        <td  style="border-bottom:1pt;solid black;font-weight:bold">''' + str(pledge_disk) + '''</td>
                        </tr>
                     '''

    def _buildFederationSites(self, site_name, site_entry):
        return ''' <tr>
                     <td colspan="3">''' + site_name + '''</td>
                     <td style="text-align:center;background:''' + TableUtils.colorize(site_entry['Avl'], '') + '''">''' + TableUtils.decorstring(site_entry['Avl']) + '''</td>
                     <td style="text-align:center;background:''' + TableUtils.colorize(site_entry['Rel'], '') + '''">''' + TableUtils.decorstring(site_entry['Rel']) + '''</td>
                     <td style="text-align:center;background:''' + '#EEEEEE' + '''">''' + TableUtils.decorstring(site_entry['Unk']) + '''</td>
                     <td style="text-align:center;background:''' + TableUtils.colorize(site_entry['Avl1'], '') + '''">''' + TableUtils.decorstring(site_entry['Avl1']) + '''</td>
                     <td style="text-align:center;background:''' + TableUtils.colorize(site_entry['Avl2'], '') + '''">''' + TableUtils.decorstring(site_entry['Avl2']) + '''</td>
                     <td style="text-align:center;background:''' + TableUtils.colorize(site_entry['Avl3'], '') + '''">''' + TableUtils.decorstring(site_entry['Avl3']) + '''</td>
                     </tr>
                  '''


class SummaryTable:
    def __init__(self, federations_stats, sorting_order):
        self.federations_stats = federations_stats
        self.sorting_order = sorting_order

    def buildSummaryTable(self):
        html = self._buildSummaryTableHeader()
        html += self._buildSummaryTableContent()
        return html

    def _buildSummaryTableHeader(self):
        return ''' <table width=100% border=1 bordercolor=white style="padding-left:1px;padding-right:1px;padding-top:2px;">
                        <tr><td width=25%><b>Federation</b></td><td width=8%><b>Availability</b></td><td width=8%><b>Reliability</b></td>
                            <td width=10%></td>
                            <td width=25%><b>Federation</b></td><td width=8%><b>Availability</b></td><td width=8%><b>Reliability</b></td>
                            <td width=8%></td>
                        </tr>
                        <tr><td colspan=3><hr/></td><td></td><td colspan=3><hr/></td><td></td></tr>
                        '''

    def _buildSummaryTableContent(self):
        html = ''
        federation_data = self._getOrderedFederations()
        mid_point = len(federation_data) - len(federation_data) // 2

        for i in range(0, mid_point):
            fed_name = federation_data[i]
            html += '<tr><td  width="25%" >' + fed_name + '''</td>
                <td style="text-align:center;background:''' + TableUtils.colorize(self.federations_stats[fed_name]['availability'], '') + '">' + TableUtils.decorstring(self.federations_stats[fed_name]['availability']) + '''</td>
                <td style="text-align:center;background:''' + TableUtils.colorize(self.federations_stats[fed_name]['reliability'], '') + '">' + TableUtils.decorstring(self.federations_stats[fed_name]['reliability']) + '</td><td width="10%"></td>'

            if i + mid_point < len(federation_data):
                fed_name = federation_data[i + mid_point]
                html += '<td width="25%" >' + fed_name + '''</td>
                    <td style="text-align:center;background:''' + TableUtils.colorize(self.federations_stats[fed_name]['availability'], '') + '">' + TableUtils.decorstring(self.federations_stats[fed_name]['availability']) + '''</td>
                    <td style="text-align:center;background:''' + TableUtils.colorize(self.federations_stats[fed_name]['reliability'], '') + '">' + TableUtils.decorstring(self.federations_stats[fed_name]['reliability']) + '</td><td/>'

        html += '</tr>'
        html += '</table>'
        return html

    def _getOrderedFederations(self):
        if self.sorting_order == 'Name':
            federations = list(self.federations_stats.keys())
            federations.sort()
            return federations

        elif self.sorting_order == 'Availability':
            all_entries = []
            for f in self.federations_stats.keys():
                all_entries.append([f, self.federations_stats[f]['availability']])
            all_entries = sorted(all_entries, key=lambda x: (-x[1], x[0]))
            sorted_stats = []
            for (name, avl) in all_entries:
                sorted_stats.append(name)
            return sorted_stats
