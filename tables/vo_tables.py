import math
from dateutil import parser
from utils.table_utils import TableUtils


class VOTable:
    def __init__(self, hist_dates, legend, state):
        self.hist_values = hist_dates
        self.legend = legend.lower()
        self.state = state.lower()

    def buildTable(self):
        border = 'border:1pt solid black;width:100;font-size:7pt;padding: 2px'
        html = '''
                <table style="width:80%;font-size:10pt" align="center">
                <tr>
                <td valign="top" style="width:100%;text-align:center;">
                <table style="font-size:10pt;width:95%;text-align:center;border-collapse: collapse;" align=center>
                    <tr style="text-align:center;background:#E6FEE6;font-weight:bold">
                        <td style="''' + border + ''';background-color:palegoldenrod">
                        Date
                        </td>
                '''
        html += self._buildContent(border)
        html += '''
                </table>
                </td>
                </tr>
                </table>
                '''
        return html

    def _buildContent(self, border):
        sites = self.hist_values.keys()
        """
        Return html string with Tier-0/1 tables for VO report
        """
        html_aux = ''
        if not sites:
            html_aux = '''
                    <tr>
                        <td>N/A</td>
                        </tr>
                        '''
        else:
            for site in sites:
                html_aux += '<td style="border:1pt solid black;width:100;font-size:5pt;padding: 2px;background-color:palegoldenrod">' + site + '<br>Avail||Unkn</td>'
            html_aux += '<td style="background:#EFFFFF;text-align:center;border:1pt solid black;width:100;font-size:5pt;padding: 2px;background-color:palegoldenrod">Average<br>Avail||Unkn</td></tr>\n'

            tempavr = [[], []]
            for date in self.hist_values[list(sites)[0]]:
                if date != 'totals_global' and date != 'totals':
                    html_aux += '<tr><td style="' + border + ';background-color:palegoldenrod">' + parser.parse(date).strftime('%d-%m') + '</td>\n'

                    daylyTotal = {'OK': 0, 'UNK': 0}

                    for site in sites:
                        upval, unkval = 0, 100
                        if date in self.hist_values[site]:
                            upval = int(self.hist_values[site][date][self.legend])
                            unkval = int(self.hist_values[site][date]['unknown'])
                        daylyTotal['OK'] += upval
                        daylyTotal['UNK'] += unkval
                        html_aux += '''
                                    <td style="''' + border + '''">
                                    <span style="float:left;color:''' + TableUtils.colorize(upval, 1) + '''"
                                    >''' + TableUtils.decorstring(upval) + '''</span>
                                    <span style="float:right;color:gray">''' + str(unkval) + '''</span>
                                    </td>
                                 '''
                    upval = int(1. * daylyTotal['OK'] / len(sites) + 0.5)
                    unkval = int(1. * daylyTotal['UNK'] / len(sites) + 0.5)
                    tempavr[0].append(upval)
                    tempavr[1].append(unkval)
                    html_aux += '''
                                <td style="background:#EFFFFF;''' + border + '''">
                                <span style="float:left;color:''' + TableUtils.colorize(upval, 1) + '''"
                                >''' + TableUtils.decorstring(upval) + '''</span>
                                <span style="float:right;color:gray">''' + str(unkval) + '''</span>
                                </td>
                                </tr>
                             '''

            html_aux += '''
                        <tr style="background:#EFFFFF;">
                        <td style="''' + border + ''';background-color:palegoldenrod">Average</td>
                     '''
            for site in sites:
                upval = self.hist_values[site]['totals'][self.legend]
                unkval = self.hist_values[site]['totals']['unknown']
                html_aux += '''
                            <td style="''' + border + '''">
                            <span style="float:left;color:''' + TableUtils.colorize(upval, 1) + '''"
                            >''' + TableUtils.decorstring(upval) + '''</span>
                            <span style="float:right;color:gray">''' + str(int(unkval + 0.5)) + '''</span>
                            </td>
                         '''
            upval = 0
            if len(tempavr[0]):
                upval = int(math.ceil((float(sum(tempavr[0]) / int(len(tempavr[0])))) * 100) / 100)
            unkval = 0
            if len(tempavr[1]):
                unkval = int(math.ceil((float(sum(tempavr[1]) / int(len(tempavr[1])))) * 100) / 100)
            html_aux += '''
                            <td style="''' + border + '''">
                            <span style="float:left;color:''' + TableUtils.colorize(upval, 1) + '''"
                            >''' + TableUtils.decorstring(upval) + '''</span>
                            <span style="float:right;color:gray">''' + str(unkval) + '''</span>
                            </td>
                         '''
            html_aux += '</tr>'

        return html_aux
