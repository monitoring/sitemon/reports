import urllib
import json
import os
import sys
from datetime import datetime
import time
import calendar
from collections import defaultdict
import click

import compare_reports_idb as idb


FILES_DEFAULT = ['WLCG_All_Sites_{}_{:%b%Y}', 'WLCG_Tier1_History_{}_{:%b%Y}', 'WLCG_Tier1_Summary_{}_{:%b%Y}', 'WLCG_Tier1_VO_{}_{:%b%Y}']
FILES = ['WLCG_All_Sites_{}_{:%b%Y}']

VO_DEFAULTS = ['ATLAS','CMS','ALICE','LHCB']

base_url = "http://wlcg-sam.cern.ch/reports/{:%Y/%Y%m}/wlcg/{}.json"
base_path = os.getcwd() + "/{}.json"

today_timestamp = calendar.timegm(datetime.utcnow().date().timetuple()) * 1000000000 # nanoseconds

def get_json_http(url):
    try:
        request = urllib.request.Request(url,headers={"Accept": "application/json"})
        sites_list_str = urllib.request.urlopen(request).read()

        return json.loads(sites_list_str)
    except Exception as e:
        # By writing to stdErr we can log this in the Flume logs
        sys.stderr.write("Error in the getting data from: " + url)
        sys.exit(-1)

def get_json_file(path):
	try:
		with open(path, 'r') as f:
			return json.load(f)

	except Exception as e:
		# By writing to stdErr we can log this in the Flume logs
		sys.stderr.write("Error in the getting data from file: " + path)
		sys.exit(-1)

def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z


def flatten_site_months(month_list):
	sites = {}
	for month in month_list:
		for site_name in month.keys():
			sites[site_name] = merge_two_dicts(month[site_name], sites.get(site_name,{}))
	return sites


def flat_file(obj):
	rtn_obj = obj
	for key in rtn_obj.keys():
		if type(obj[key]) is list:
			rtn_obj[key] = flatten_site_months(obj[key])
	return rtn_obj


def join_new_old_reports(old, new):
	output = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict())))
	for kind in old.keys():
		for site in old[kind].keys():
			for day in old[kind][site].keys():
				output[kind][site][day]["old"] = old[kind][site][day]

	for kind in new.keys():
		for site in new[kind].keys():
			for day in new[kind][site].keys():
				output[kind][site][day]["new"] = new[kind][site][day]
	return output


def compare_reports(vos_old, vos_new, date):
	joined_reports = {}
	for vo in VO_DEFAULTS:
		joined_reports[vo] = {}
		for file in FILES:
			file_name = file.format(vo, date)
			vos_old[vo][file_name] = flat_file(vos_old[vo][file_name])
			vos_new[vo][file_name] = flat_file(vos_new[vo][file_name])

			joined_reports[vo][file_name] = join_new_old_reports(vos_old[vo][file_name], vos_new[vo][file_name])

	return joined_reports


def calculate_status(elem):
	obj = {"status": "OK"}
	if "new" in elem.keys() and "old" in elem.keys():
		if type(elem) == dict:
			for key in elem["new"]:
				if elem["new"][key] != elem["old"][key]:
					obj["status"] = "There are differences!"
		else:
			if elem["new"] != elem["old"]:
				obj["status"] = "There are differences!"

	if "old" not in elem.keys():
		obj["status"] = "New report is present, old not!"
	if "new" not in elem.keys():
		obj["status"] = "Old report is present, new not!"

	obj["data"] = elem
	return obj


def calculate_status_federation(elem):
	obj = {"status": "No status for Federation"}

	obj["data"] = elem
	return obj


def calculate_diff(elem):
	diff = {
		"new-old-CRIT" : None,
		"new-old-OK" : None,
		"new-old-SCHED" : None,
		"new-old-UNKNOWN" : None
		}

	if "new" in elem.keys() and "old" in elem.keys():
		diff["new-old-CRIT"] = elem["new"]["CRIT"] - elem["old"]["CRIT"]
		diff["new-old-OK"] = elem["new"]["OK"] - elem["old"]["OK"]
		diff["new-old-SCHED"] = elem["new"]["SCHED"] - elem["old"]["SCHED"]
		diff["new-old-UNKNOWN"] = elem["new"]["UNKNOWN"] - elem["old"]["UNKNOWN"]
	return diff


def calculate_diff_federation(elem, tipe):
	if tipe == "pledges":
		return diff_pledges(elem)
	if tipe == "sites":
		return diff_sites(elem)
	return diff_standard(elem)


def diff_standard(std):
	diff = std
	diff["new-old"] = None

	if "new" in std.keys() and "old" in std.keys():
		diff["new-old"] = std["new"] - std["old"]
	return diff


def diff_pledges(pledges):
	diff = {}

	if "new" in pledges.keys():
		for key in pledges["new"].keys():
			diff["new-"+key] = pledges["new"][key]
	if "old" in pledges.keys():
		for key in pledges["old"].keys():
			if pledges["old"][key] != "":
				diff["old-"+key] = int(pledges["old"][key])
			else:
				diff["old-"+key] = None
	if "new" in pledges.keys() and "old" in pledges.keys():
		for key in pledges["new"].keys():
			if pledges["new"][key] != None and pledges["old"][key] != "":
				diff["new-old-"+key] = diff["new-"+key] - diff["old-"+key]

	return diff

def diff_sites(sites):
	diff = defaultdict(lambda: defaultdict())

	if "new" in sites.keys():
		for site in sites["new"].keys():
			for key in sites["new"][site].keys():
				diff[site]["new-"+key] = sites["new"][site][key]
	if "old" in sites.keys():
		for site in sites["old"].keys():
			for key in sites["old"][site].keys():
				if key != "Unk":
					diff[site]["old-"+key] = float(sites["old"][site][key])
				else:
					diff[site]["old-"+key] = float(sites["old"][site][key].replace("%","")) # Transform the percentage

	return diff


def make_stats(report, date):
	stats = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict())))))))
	for vo in VO_DEFAULTS:
		for file in FILES:
			file_name = file.format(vo, date)
			for kind in report[vo][file_name].keys():
				for site in report[vo][file_name][kind].keys():
					for day in report[vo][file_name][kind][site].keys():
						elem = report[vo][file_name][kind][site][day]
						if kind != "Federation Stats":
							stats[vo][file_name][kind][site][day] = calculate_status(elem)
							stats[vo][file_name][kind][site][day]["data"]["new-old"] = calculate_diff(stats[vo][file_name][kind][site][day]["data"])
						else:
							stats[vo][file_name][kind][site][day] = calculate_status_federation(elem)
							stats[vo][file_name][kind][site][day]["data"] = calculate_diff_federation(stats[vo][file_name][kind][site][day]["data"], day)
	return stats


def compute_comparison(month, idb_user, idb_password, original):
    if original:
        global base_url
        base_url = "http://wlcg-sam.cern.ch/reports/{:%Y/%Y%m}/original/wlcg/{}.json"

    report_month = datetime.strptime(month, '%Y-%m')
    vos_old = {}
    vos_new = {}

    for vo in VO_DEFAULTS:
        vos_old[vo] = {}
        vos_new[vo] = {}
        for file in FILES:
            file_name = file.format(vo, report_month)
            url = base_url.format(report_month, file_name)
            file_path = base_path.format(file_name)
            vos_old[vo][file_name] = get_json_http(url)
            vos_new[vo][file_name] = get_json_file(file_path)

    joined_report = compare_reports(vos_old, vos_new, report_month)
    stats = make_stats(joined_report, report_month)

    rows = idb.transform_stats_to_influx(VO_DEFAULTS, FILES, stats, report_month, today_timestamp)

    #print(json.dumps(rows["std"]))
    #print(json.dumps(rows["federation"]))
    #print(json.dumps(rows["exectime"]))

    idb.send_data_to_db(rows["std"], idb_user, idb_password)
    idb.send_data_to_db_exec(rows["exectime"], idb_user, idb_password)
    idb.send_data_to_db_fed(rows["federation"], idb_user, idb_password)


@click.command()
@click.option("--idb-user", prompt='IDB user', help='InfluxDB username')
@click.option("--idb-password", prompt='IDB password', help='InfluxDB password')
@click.option("--month", prompt='Month', help='Month in the format YYYY-MM')
def main(idb_user, idb_password, month):
	compute_comparison(month, idb_user, idb_password)

if __name__ == '__main__':
    main()
