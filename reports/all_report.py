from .report import Report
from utils.create_json import createJson, createFedJson
from pages.fedhistpage import FedHistPage
from pages.fedsummarypage import FedSummaryByNamePage, FedSummaryByAvailabilityPage
from pages.t0t1page import T0T1Page
from utils.report_utils import getDates, getRangedData_t2
from dateutil import parser
from dateutil.relativedelta import relativedelta
from dao import ETFDAO


import json


class AllReport(Report):

    def __init__(self, name, es_user, es_password, es_max_buckets, vo, start_time, end_time, month, only_one_month):
        Report.__init__(self, name, es_user, es_password, es_max_buckets, vo, start_time, end_time, month, 'Landscape')
        self.all_tiers = self.idb_connector.getDailySites(n_tiers=2)

        # Select only one month to be faster (current month)
        self.only_one_month = only_one_month
        if self.only_one_month:
            self.federation_data = self.idb_connector.getHistoricalFederationsWithPledgesFor(past_months=0)
        else:
            self.federation_data = self.idb_connector.getHistoricalFederationsWithPledgesFor(past_months=3)

        self.federation_title = 'Tier-2 Availability and Reliability Report'
        self.fed_page = FedHistPage(self, self.federation_title, self.federation_data, '', only_one_month)

    def _createJson(self):
        json_map = {}
        #CERN-PROD needs to be computed also
        t1_sites = self.all_tiers[0]
        t1_sites.update(self.all_tiers[1])

        json_map["Availability T1"] = createJson(t1_sites, "availability")
        json_map["Reliability T1"] = createJson(t1_sites, "reliability")

        summary_data = self.fed_page._generateFederationSummary(self.federation_data)
        json_map["Federation Stats"] = createFedJson(self.federation_data, summary_data)

        if not self.only_one_month:
            parsed_date = parser.parse(self.end_time, dayfirst=False, yearfirst=True)
            first_date = (parsed_date - relativedelta(months=4)).strftime("%Y-%m-%dT00:00:00Z")
        else:
            first_date = self.start_time

        new_idb_conn = ETFDAO(self.es_user, self.es_password, self.es_max_buckets, self.vo, first_date, self.end_time)
        t2_sites = new_idb_conn.getDailySites(n_tiers=3)[2]

        dates = getDates(t2_sites)
        t2_availability_data = getRangedData_t2(self.es_user, self.es_password, self.es_max_buckets, dates, self.vo)

        json_map["Availability T2"] = []
        for month in dates:
            json_map["Availability T2"].append(createJson(t2_availability_data[month], "reliability"))

        return json_map

    def _createBody(self):
        body = '''<body>'''

        t0_t1_sites = self.all_tiers[0]
        t0_t1_sites.update(self.all_tiers[1])

        # Create Tier-0/1 Availability/Reliability pages
        for legend in ['Availability', 'Reliability']:
            page = T0T1Page(self, t0_t1_sites, '%s of WLCG Tier-0 + Tier-1 Sites' % (legend), legend)
            body += page.html

        # Create Tier 2 Federation pages
        federation_data_current = self.idb_connector.getHistoricalFederationsFor(past_months=1)
        body += FedSummaryByNamePage(self, self.federation_title, federation_data_current, '').html
        body += FedSummaryByAvailabilityPage(self, self.federation_title, federation_data_current, '').html
        body += self.fed_page.html

        body += self._createFooter()
        body += '''</body>'''

        return body
