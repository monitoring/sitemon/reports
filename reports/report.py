from io import StringIO
import json

from dao import ETFDAO
import pdfkit

class Report:

    def __init__(self, name, es_user, es_password, es_max_buckets, vo, start_time, end_time, month, pdf_orientation):
        self.name = name
        self.es_user = es_user
        self.es_password = es_password
        self.es_max_buckets = es_max_buckets
        self.vo = vo
        self.start_time = start_time
        self.end_time = end_time
        self.month = month
        self.idb_connector = ETFDAO(es_user, es_password, es_max_buckets, vo, start_time, end_time)
        self.pdf_orientation = pdf_orientation

        # TODO: Get the profiles from a database?
        if vo == 'ATLAS':
            self.profile = '(CREAM-CE + ARC-CE + HTCONDOR-CE + GLOBUS) * (all SRMv2 + all SRM + all GRIDFTP)'
        elif vo == 'CMS':
            self.profile = '(CREAM-CE + ARC-CE + HTCONDOR-CE) * all SRM'
        elif vo == 'LHCB':
            self.profile = 'CREAM-CE + ARC-CE + HTCONDOR-CE + VAC'
        elif vo == 'ALICE':
            self.profile = '@ALICE_CE * @ALICE_VOBOX * all AliEn-SE'

        # TODO: Probably not needed
        self.request = "wlcg-sam.cern.ch"

    def generateHtmlReport(self, month_file):
        html = self._createHTML()

        with open('./'+self.name+'_'+self.vo+'_'+month_file+'.html', 'w') as report_html:
            report_html.write(html)

        options = {
            'quiet': '',
            'orientation': self.pdf_orientation,
            'margin-top': '1cm',
            'margin-bottom': '1cm',
            'footer-center': '[page]',
            'enable-local-file-access': "",
            'print-media-type': "",
        }
        html_file = f"./{self.name}_{self.vo}_{month_file}.html"
        pdf_file = f"./{self.name}_{self.vo}_{month_file}.pdf"
        print("Generating HTML report")
        print(html_file)
        print(pdf_file)
        pdfkit.from_file(html_file, pdf_file, options=options)

    def generateJsonReport(self, month_file):
        json_data = self._createJson()

        with open('./'+self.name+'_'+self.vo+'_'+month_file+'.json', 'w') as report_json:
            json.dump(json_data, report_json)

    def _createHTML(self):
        html = '''<html>'''
        html += self._createHeader()
        html += self._createBody()
        html += '''</html>'''

        return html

    def _createHeader(self):
        header = '''<head>'''
        header += self._defineLayoutStyle()
        header += '''</head>'''

        return header

    def _createFooter(self):
        return '''
            <div id="footerContent" style="text-align:center" >
            </div>'''

    def _defineLayoutStyle(self):
        layouts = ['landscape']
        if (self.name == 'vo'):
            layouts = ['landscape', 'portrait']
        elif (self.name == 'history'):
            layouts = ['portrait']

        style = '''  <style>'''

        for l in layouts:
            style += '@page ' + l + '''_template {
                          size: a4 ''' + l + ''' ;
                          margin: 1cm;
              @frame footer {
                -pdf-frame-content: footerContent;
                height: 1cm;
                bottom: 0cm;
                margin-left: 14cm;
                margin-right: 5cm;
                text-align: center;
              }'''

        style += '''  </style>
              <!-- switch page templates -->
              <pdf:nexttemplate name="''' + layouts[-1] + '''_template" ></pdf:nexttemplate>
                        <link rel="stylesheet" type="text/css" href="www/css/sam3reports.css"/>'''

        return style
