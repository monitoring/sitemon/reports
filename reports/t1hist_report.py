from .report import Report
from pages.t1historypage import T1HistoryPage
from utils.create_json import createJson
from utils.report_utils import getDates, getRangedData_t0t1
from dateutil import parser
from dateutil.relativedelta import relativedelta
from dao import ETFDAO


class T1HistoryReport(Report):

    def __init__(self, name, es_user, es_password, es_max_buckets, vo, start_time, end_time, month, only_one_month):
        Report.__init__(self, name, es_user, es_password, es_max_buckets, vo, start_time, end_time, month, 'Portrait')

        # Select only one month to be faster (current month)
        self.only_one_month = only_one_month
        if self.only_one_month:
            first_date = start_time
        else:
            parsed_date = parser.parse(end_time, dayfirst=False, yearfirst=True)
            first_date = (parsed_date - relativedelta(months=6)).strftime("%Y-%m-%dT00:00:00Z")

        self.idb_connector = ETFDAO(es_user, es_password, es_max_buckets, vo, first_date, end_time)

    def _createJson(self):
        t1_sites = self.idb_connector.getDailySites(n_tiers=2)[1]
        dates = getDates(t1_sites)
        ranged_data = getRangedData_t0t1(self.es_user, self.es_password, self.es_max_buckets, dates, self.vo)

        reliability_map = {"T1 Reliabilities": []}
        for month in dates:
            reliability_map["T1 Reliabilities"].append(createJson(ranged_data[month], "reliability"))
        return reliability_map

    def _createBody(self):
        body = '''<body>'''
        data = self.idb_connector.getT0T1MonthlyReliability()

        # Create Tier-1 Reliability pages
        page = T1HistoryPage(self, data)
        body += page.html
        body += self._createFooter()
        body += '''</body>'''

        return body
