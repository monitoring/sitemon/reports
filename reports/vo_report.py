from .report import Report
from pages.t0t1page import T0T1Page
from pages.vopage import VOPage
from utils.create_json import createJson
from utils.report_utils import getDates, getRangedData_t0t1
from dateutil import parser
from dateutil.relativedelta import relativedelta
from dao import ETFDAO

import json


class VOReport(Report):

    def __init__(self, name, es_user, es_password, es_max_buckets, vo, start_time, end_time, month, only_one_month):
        Report.__init__(self, name, es_user, es_password, es_max_buckets, vo, start_time, end_time, month, 'Portrait')

        # Select only one month to be faster (current month)
        self.only_one_month = only_one_month
        if self.only_one_month:
            self.first_date = start_time
        else:
            parsed_date = parser.parse(end_time, dayfirst=False, yearfirst=True)
            self.first_date = (parsed_date - relativedelta(months=6)).strftime("%Y-%m-%dT00:00:00Z")

        self.idb_connector = ETFDAO(es_user, es_password, es_max_buckets, vo, self.first_date, end_time)

        self.t0_t1_sites = self._t0t1data(self.idb_connector.getDailySites(n_tiers=2))
        self.dates = getDates(self.t0_t1_sites)
        self.dates.reverse()
        self.t0_t1_site_ranged = getRangedData_t0t1(self.es_user, self.es_password, self.es_max_buckets, self.dates, self.vo)

    def _createJson(self):
        json_map = {'Availability': [], 'Reliability': []}
        for month in self.dates:
            for legend in json_map.keys():
                json_map[legend].append(createJson(self.t0_t1_site_ranged[month], legend.lower()))
        return json_map

    def _createBody(self):
        body = '''<body>'''

        # Get first the Availability table for all months in Range
        self.month = "%s - %s" % (parser.parse(self.dates[-1]).strftime('%b-%Y'), parser.parse(self.dates[0]).strftime('%b-%Y'))
        page = T0T1Page(self, self.t0_t1_sites, 'Availability of WLCG Tier-0 + Tier-1 Sites', 'Availability', start_time=self.first_date)
        body += page.html
        body += '<br>'

        # Get Availability and Reliability tables for each month in Range
        for month in self.dates:
            # Dont try to generate report for empty months
            if self.t0_t1_site_ranged[month]:
                start_datetime = parser.parse(month).replace(day=1)
                end_datetime = start_datetime + relativedelta(months=1)
                self.month = start_datetime.strftime('%b-%Y')
                for legend in ('Availability', 'Reliability'):
                    body += T0T1Page(self, self.t0_t1_site_ranged[month], '%s of WLCG Tier-0 + Tier-1 Sites' % (legend),
                                     legend, start_time=start_datetime.strftime("%Y-%m-%dT00:00:00Z"), end_time=end_datetime.strftime("%Y-%m-%dT00:00:00Z")).html
                    body += VOPage(self, self.t0_t1_site_ranged[month], legend).html

        body += self._createFooter()
        body += '''</body>'''

        return body

    def _t0t1data(self, all_tiers):
        t0_t1_sites = all_tiers[0]
        t0_t1_sites.update(all_tiers[1])

        return t0_t1_sites
