from .report import Report
from pages.t0t1page import T0T1Page
from utils.create_json import createJson


class T1SummaryReport(Report):

    def __init__(self, name, es_user, es_password, es_max_buckets, vo, start_time, end_time, month, only_one_month):
        Report.__init__(self, name, es_user, es_password, es_max_buckets, vo, start_time, end_time, month, 'Landscape')
        self.all_tiers = self.idb_connector.getDailySites(n_tiers=2)
        self.t0t1_sites = self.all_tiers[0]
        self.t0t1_sites.update(self.all_tiers[1])

    def _createJson(self):
        availability_map = {}
        availability_map["T1 Availabilities"] = createJson(self.t0t1_sites, "availability")
        return availability_map

    def _createBody(self):
        body = '''<body>'''

        # Create Tier-1 Availability pages
        page = T0T1Page(self, self.t0t1_sites, 'Availability of WLCG Tier-0 + Tier-1 Sites', 'Availability')
        body += page.html
        body += self._createFooter()
        body += '''</body>'''

        return body
