# sam3-reports

Report generation tool for SAM3 availability and reliability data.

## Dependencies

The python dependencies can be acquired for the requirements file by using:

```
pip install -r requirements.txt
```

There is an extra package that's needed to be installed beforehand:

```
yum install wkhtmltopdf
```

## CLI

For the time being it's quite limited on the options, please check inside the code. Here is an example of a running command line

```
python generate_reports.py --vo_name ATLAS --month "2020-01" --all
```
