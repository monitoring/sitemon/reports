from influxdb import InfluxDBClient
from datetime import datetime
import time
import calendar


idb_map = {
	"idb_hostname" : "dbod-m-xsls-d.cern.ch",
	"idb_port" : "8080",
	"idb_user" : "***",
	"idb_password" : "***",
	"idb_database" : "monit_qa_monitoring"
};

field_dict = {
	"new-CRIT" : "new-CRIT",
	"new-UNKNOWN" : "new-UNKNOWN",
	"new-OK": "new-OK",
	"new-SCHED": "new-SCHED",
	"old-CRIT" : "old-CRIT",
	"old-UNKNOWN" : "old-UNKNOWN",
	"old-OK": "old-OK",
	"old-SCHED": "old-SCHED"
};

# Same fields for the different comparison based on exec time
field_dict_exec = field_dict

field_dict_fed = {
	# Sites and others
	"new-Avl": "new-Avl",
	"new-Unk": "new-Unk",
	"new-Rel": "new-Rel",
	"old-Avl": "old-Avl",
	"old-Rel": "old-Rel",
	"old-Unk": "old-Unk",

	# Pledges
	"new-DISK": "new-DISK",
	"old-DISK": "old-DISK",
	"new-CPU": "new-CPU",
	"old-CPU": "old-CPU",
	"old-TAPE": "old-TAPE"
};

tag_dict = {
	"vo": "vo",
	#"file": "file",
	"type": "type",
	"site": "site"
};

tag_dict_exec = {
	"vo": "vo",
	#"file": "file",
	"year": "year",
	"month": "month",
	"day": "day",
	"type": "type",
	"value": "value",
	"site": "site"
};

tag_dict_fed = {
	"vo": "vo",
	#"file": "file",
	"year": "year",
	"month": "month",
	"type": "type",
	"federation": "federation",
	"value": "value",
	"site": "site"
};

top_dict = {
    'timestamp': 'time'
}

def send_data_to_db(rows, idb_user, idb_password):
    send_to_influx(rows, "compare_sam_reports", tag_dict, field_dict, idb_user, idb_password)

def send_data_to_db_exec(rows, idb_user, idb_password):
    send_to_influx(rows, "compare_sam_reports_exectime", tag_dict_exec, field_dict_exec, idb_user, idb_password)

def send_data_to_db_fed(rows, idb_user, idb_password):
    send_to_influx(rows, "compare_sam_reports_federation", tag_dict_fed, field_dict_fed, idb_user, idb_password)
    

def send_to_influx(rows, measurement, tags, fields, idb_user, idb_password):
	influxdb = InfluxDBClient(idb_map["idb_hostname"],
                          idb_map["idb_port"],
                          idb_user,
                          idb_password,
                          idb_map["idb_database"], True, False)

	idb_items = []
	for row in rows:
		idb_item = {}
		idb_item['measurement'] = measurement
		idb_item['tags'] = {}
		idb_item['fields'] = {}

		for i, value in enumerate(row):
			if value in top_dict:
				idb_item[top_dict[value]] = row[value]
			if value in tags:
				idb_item['tags'][tags[value]] = row[value]
			if value in fields:
				idb_item['fields'][fields[value]] = row[value]
		idb_items.append(idb_item)
	influxdb.write_points(points=idb_items, retention_policy='raw')


def prepare_influx_document(data, current_year, day, vo, file_name, kind, site):
	current_day = datetime.strptime(day + "-" + str(current_year), '%d-%m-%Y')
	elem = data

	obj = {
		"new-CRIT" : None,
		"new-UNKNOWN" : None,
		"new-OK": None,
		"new-SCHED": None,
		"old-CRIT" : None,
		"old-UNKNOWN" : None,
		"old-OK": None,
		"old-SCHED": None,
		"vo": vo,
		#"file": file_name,
		"type": kind,
		"site": site,
		"timestamp": calendar.timegm(current_day.timetuple()) * 1000000000 # nanoseconds
	};

	if "new" in elem:
		obj["new-CRIT"] = elem["new"]["CRIT"]* 1.0
		obj["new-UNKNOWN"] = elem["new"]["UNKNOWN"]* 1.0
		obj["new-OK"] = elem["new"]["OK"]* 1.0
		obj["new-SCHED"] = elem["new"]["SCHED"]* 1.0
	if "old" in elem:
		obj["old-CRIT"] = elem["old"]["CRIT"]* 1.0
		obj["old-UNKNOWN"] = elem["old"]["UNKNOWN"]* 1.0
		obj["old-OK"] = elem["old"]["OK"]* 1.0
		obj["old-SCHED"] = elem["old"]["SCHED"]* 1.0
	return obj

def prepare_influx_document_exectime(data, current_year, day, vo, file_name, kind, site, today_timestamp):
	current_day = datetime.strptime(day + "-" + str(current_year), '%d-%m-%Y')
	elem = data

	obj = {
		"new-CRIT" : None,
		"new-UNKNOWN" : None,
		"new-OK": None,
		"new-SCHED": None,
		"old-CRIT" : None,
		"old-UNKNOWN" : None,
		"old-OK": None,
		"old-SCHED": None,
		"vo": vo,
		#"file": file_name,
		"type": kind,
		"site": site,
        "year": current_day.year,
        "month": current_day.month,
        "day": current_day.day,
		"timestamp": today_timestamp # nanoseconds
	};

	if "new" in elem:
		obj["new-CRIT"] = elem["new"]["CRIT"]* 1.0
		obj["new-UNKNOWN"] = elem["new"]["UNKNOWN"]* 1.0
		obj["new-OK"] = elem["new"]["OK"]* 1.0
		obj["new-SCHED"] = elem["new"]["SCHED"]* 1.0
	if "old" in elem:
		obj["old-CRIT"] = elem["old"]["CRIT"]* 1.0
		obj["old-UNKNOWN"] = elem["old"]["UNKNOWN"]* 1.0
		obj["old-OK"] = elem["old"]["OK"]* 1.0
		obj["old-SCHED"] = elem["old"]["SCHED"]* 1.0
	return obj

def prepare_influx_document_fed(data, current_year, month, vo, file_name, kind, federation, value, site, today_timestamp):
	current_day = datetime.strptime(str(month) + "-" + str(current_year), '%m-%Y')
	elem = data

	obj = {
		"new-Avl": None,
		"new-Unk": None,
		"old-Unk": None,
		"old-Rel": None,
		"old-Avl": None,
		"new-Rel": None,

		# Pledges not used
		#"new-DISK": None,
		#"old-DISK": None,
		#"new-CPU": None,
		#"old-CPU": None,
		#"old-TAPE": None,

		"vo": vo,
		#"file": file_name,
		"type": kind,
		"federation": federation,
		"value": value,
		"site": site,
        "year": current_day.year,
        "month": current_day.month,
		"timestamp": today_timestamp # nanoseconds
	}

	valid = False

	if value in ["Avl","Rel"]:
		for key in data:
			if data[key] != None:
				obj[key+"-"+value] = data[key] * 1.0
				valid = True
			else:
				obj[key+"-"+value] = None
	
	if value == "pledges":
		for key in data:
			if data[key] != None:
				obj[key] = data[key] * 1.0
				valid = True
			else:
				obj[key] = None

	if value == "sites":
		for key in data:
			if data[key] != None:
				obj[key] = data[key] * 1.0
				valid = True
			else:
				obj[key] = None
	
	return obj if valid else None


def _prepare_federation_comparison(file_names, rows, current_year, date, vo,
                                   file_name, kind, today_timestamp):
    for federation in file_names.keys():
        for value in file_names[federation].keys():
            if value == "sites":
                for site in file_names[federation]["sites"]["data"].keys():
                    data = file_names[federation][value]["data"][site]

                    # For this one we use current year
                    obj = prepare_influx_document_fed(data, current_year, date.month,
                                                      vo, file_name, kind, federation,
                                                      value, site, today_timestamp)
                    if obj != None: # TODO:  mapping for next years
                        rows["federation"].append(obj)


def _prepare_sites_comparison(file_names, rows, current_year, date, vo,
                              file_name, kind, today_timestamp):
    for site in file_names.keys():
        for day in file_names[site].keys():
            data = file_names[site][day]["data"]
            # For this one we use the report year
            rows["std"].append(prepare_influx_document(data, date.year, day,
                                                       vo, file_name, kind, site))

            # For this one we use current year
            rows["exectime"].append(
                prepare_influx_document_exectime(data, current_year, day, vo,
                                                 file_name, kind, site, today_timestamp))



def transform_stats_to_influx(vos, files, stats, date, today_timestamp):
    current_year = datetime.now().year
    rows = {
        "std": [],
        "federation": [],
        "exectime": [] # Calculation for execution time with tags YEAR MONTH DAY
    }

    for vo in vos:
        for file in files:
            file_name = file.format(vo, date)
            for kind in stats[vo][file_name].keys():
                if kind != "Federation Stats": # Site report
                    _prepare_sites_comparison(stats[vo][file_name][kind], rows, current_year, date, vo,
                                              file_name, kind, today_timestamp)
                else: # Federation report
                    _prepare_federation_comparison(stats[vo][file_name][kind], rows, current_year, date,
                                                   vo, file_name, kind, today_timestamp)

    return rows
