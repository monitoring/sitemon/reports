import copy
import calendar

from elasticsearch import Elasticsearch, helpers
from elasticsearch_dsl import Search, A
from datetime import datetime


class ElasticSource():

    index = "monit_prod_sitemon_agg_site-stats*"

    format = {"month": "%Y-%m", "day": "%Y-%m-%d"}

    def __init__(self, es_user, es_password, es_max_buckets, vo, start_time, end_time):
        self.es_max_buckets = es_max_buckets
        self.vo = vo.lower()
        self.start_time = start_time
        self.end_time = end_time
        self.es = Elasticsearch(['monit-opensearch-lt.cern.ch:443/os'],
                   http_auth=(es_user, es_password),
                   scheme='https', port=443,
                   verify_certs=False, timeout=60)


    def _query_es(self, tier, vo, interval, start_time, end_time):
        start_dt = datetime.strptime(start_time, '%Y-%m-%dT%H:%M:%SZ')
        end_dt = datetime.strptime(end_time, '%Y-%m-%dT%H:%M:%SZ')
        start_ts = calendar.timegm(start_dt.timetuple()) * 1000
        end_ts = calendar.timegm(end_dt.timetuple()) * 1000
        q = {
          'bool': { 
            'must': [
                {'match': { 'data.dst_tier': tier}},
                {'match': { 'data.vo': vo}},
                {'match': { 'data.profile': '%s_CRITICAL' % vo.upper()}},
                {'range': {'metadata.timestamp': {"gte": start_ts,"lt": end_ts}}}
            ],
            'must_not': [
                {'match': {'data.dst_federation': 'UNKNOWN'}},
                {'match': {'data.dst_experiment_site': 'BNLLAKE'}},
                {'match': {'data.dst_federation': 'FR-IN2P3-SUBATECH'}},
                {'match': {'data.dst_federation': 'TW-FTT-T2'}},
                {'match': {'data.dst_federation': 'RU-RDIG'}},
                {'match': {'data.dst_federation': 'NRC-KI-T1'}},
            ]
          }
        }

        key_aggs = [
            {'site': A('terms', field='data.dst_experiment_site')},
            {'federation': A('terms', field='data.dst_federation')},
            {'date': A('date_histogram', field='metadata.timestamp', interval=interval)}
        ]

        s = Search(using=self.es, index=self.index).query(q)
        s.aggs.bucket('comp', 'composite', sources=key_aggs, size = self.es_max_buckets) \
              .metric('availability', 'avg', field='data.availability') \
              .metric('reliability', 'avg', field='data.reliability') \
              .metric('unknown', 'avg', field='data.unknown')

        return s.execute()

    def getDataByTier(self, tier, interval, start_time, end_time, ignore_federation=False):

        se = self._query_es(tier, self.vo, interval, start_time, end_time)
        results = {}
        for comb in se.aggs['comp']:
            site = comb.key.site
            fede = comb.key.federation
            if ignore_federation:
                key = site
            else:
                key = (site, fede)
            if key not in results:
                results[key] = {}
                results[key]['site'] = site
                results[key]['federation'] = fede
                results[key]['date'] = {}
            date = datetime.fromtimestamp(comb.key.date / 1000).strftime(self.format[interval])
            results[key]['date'][date] = {}
            results[key]['date'][date]['availability'] = comb['availability']['value']
            results[key]['date'][date]['reliability'] = comb['reliability']['value']
            results[key]['date'][date]['unknown'] = comb['unknown']['value']
        return results
