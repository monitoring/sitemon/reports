import click
from datetime import datetime
from dateutil.relativedelta import relativedelta
from reports.all_report import AllReport
from reports.t1hist_report import T1HistoryReport
from reports.t1summary_report import T1SummaryReport
from reports.vo_report import VOReport

VO_DEFAULTS = ['ATLAS', 'CMS', 'ALICE', 'LHCB']
PATTERN_MONTH = '{:%b%Y}'
PATTERN_MONTH_REPORTS = '{:%B %Y}'


@click.command()
@click.option("--es_user", required=True, default="***", help='ElasticSearch username')
@click.option("--es_password", required=True, default="***", help='ElasticSearch password')
@click.option("--es_max_buckets", required=False, default=10000, help='ElasticSearch max buckets per aggregation')
@click.option("--vo_name", help='VO to compute in the report', default=None)
@click.option("--no_json", is_flag=True, help='Flag to disable json output')
@click.option("--no_html", is_flag=True, help='Flag to disable html output')
@click.option("--all", is_flag=True, help='Flag to produce all the reports')
@click.option("--full_report", is_flag=True, help='Flag to produce only the full report')
@click.option("--historical_report", is_flag=True, help='Flag to produce only the historical report')
@click.option("--summary_report", is_flag=True, help='Flag to produce only the summary report')
@click.option("--vo_report", is_flag=True, help='Flag to produce only the vo report')
# @click.option("--pdf/--no-pdf", default=False)
@click.option("--month", prompt='Month', help='Month in the format YYYY-MM')
def init(es_user, es_password, es_max_buckets, vo_name, month, no_json, no_html, all, full_report, historical_report, summary_report, vo_report):
    if all:
        generateReport(es_user, es_password, es_max_buckets, vo_name, month, no_json, no_html, True, True, True, True)
    else:
        generateReport(es_user, es_password, es_max_buckets, vo_name, month, no_json, no_html, full_report, historical_report, summary_report, vo_report)

def generateReport(es_user, es_password, es_max_buckets, vo_name, month, no_json, no_html, full_report, historical_report, summary_report, vo_report):
    date = datetime.strptime(month, '%Y-%m')

    start_time = (date).strftime("%Y-%m-%dT00:00:00Z")
    end_time = (date + relativedelta(months=1)).strftime("%Y-%m-%dT00:00:00Z")

    # To avoid to produce the reports for all the months, it's necessary only for the month as parameter
    only_one_month = no_html
    
    month_file = PATTERN_MONTH.format(date)
    month_reports = PATTERN_MONTH_REPORTS.format(date)

    if vo_name is None:
        vos = VO_DEFAULTS
    else:
        vos = [vo_name]
    print("Processing VOs:", vos)
    for vo in vos:
        print("Starting reports vo - " + vo + "...")

        if full_report:
            print("Starting full report")
            all_report = AllReport("WLCG_All_Sites", es_user, es_password, es_max_buckets, vo, start_time, end_time, month_reports, only_one_month)
            if not no_html:
                all_report.generateHtmlReport(month_file)
            if not no_json:
                all_report.generateJsonReport(month_file)

        if historical_report:
            print("Starting Historical report")
            hist_report = T1HistoryReport("WLCG_Tier1_History", es_user, es_password, es_max_buckets, vo, start_time, end_time, month_reports, only_one_month)
            print(hist_report)
            if not no_html:
                hist_report.generateHtmlReport(month_file)
            if not no_json and not only_one_month:
                hist_report.generateJsonReport(month_file)


        if summary_report:
            print("Starting Summary report")
            summary_report = T1SummaryReport("WLCG_Tier1_Summary", es_user, es_password, es_max_buckets, vo, start_time, end_time, month_reports, only_one_month)
            if not no_html:
                summary_report.generateHtmlReport(month_file)
            if not no_json and not only_one_month:
                summary_report.generateJsonReport(month_file)

        if vo_report:
            print("Starting VO report")
            vo_report = VOReport("WLCG_Tier1_VO", es_user, es_password, es_max_buckets, vo, start_time, end_time, month_reports, only_one_month)
            if not no_html:
                vo_report.generateHtmlReport(month_file)
            if not no_json and not only_one_month:
                vo_report.generateJsonReport(month_file)

    print("Finished reports vo - " + vo + "...")


if __name__ == '__main__':
    init()
