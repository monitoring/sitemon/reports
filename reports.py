import click
import re
import os
import shutil
import generate_reports
import compare_reports

from datetime import datetime
from datetime import date
import time
from dateutil.relativedelta import relativedelta

today = datetime.now()


def run_comparison(idb_user, idb_password, past_months, original):

    if os.getenv('ES_USER'):
        es_user = os.getenv('ES_USER')

    if os.getenv('ES_PASSWORD'):
        es_password = os.getenv('ES_PASSWORD')

    if os.getenv('IDB_USER'):
        idb_user = os.getenv('IDB_USER')

    if os.getenv('IDB_PASSWORD'):
        idb_password = os.getenv('IDB_PASSWORD')

    if idb_user == "***" or idb_user == "***":
        print("Error: Influxdb user or password not provided!")
    else:
        for x in range(1, past_months+1):
            report_month = (today - relativedelta(months=x)).strftime("%Y-%m")
            print("Computing report for {0}".format(report_month))
            run_per_month_comparison(es_user, es_password, idb_user, idb_password, report_month, original)


def run_per_month_comparison(es_user, es_password, idb_user, idb_password, report_month, original):
    no_json = False
    no_html = True
    print("Starting generating reports...")
    generate_reports.generateReport(es_user, es_password, None, report_month, no_json, no_html, True, True, True, True)
    print("Finished generating reports...")

    print("Starting comparing reports...")
    compare_reports.compute_comparison(report_month, idb_user, idb_password, original)
    print("Finished comparing reports...")


def run_report_generation(es_user, es_password, es_max_buckets, output_path, past_months, final):

    if os.getenv('ES_USER'):
        es_user = os.getenv('ES_USER')

    if os.getenv('ES_PASSWORD'):
        es_password = os.getenv('ES_PASSWORD')

    if es_user == "***" or es_user == "***":
        print("Error: ElasticSearch user or password not provided!")
    else:
        month = (today - relativedelta(months=past_months))
        report_month = month.strftime("%Y-%m")
        print("Starting generating reports...")
        generate_reports.generateReport(es_user, es_password, es_max_buckets, None, report_month, False, False, True, True, True, True)
        print("Finished generating reports...")

        print("Moving them to output folder...")
        _arrange_files(output_path, month, final)
        print("Finished moving reports")


def _arrange_files(output_path, month, final):
    last_subfolder = "final" if final else "drafts"
    monthly_path = os.path.join(output_path, month.strftime("%Y"), month.strftime("%Y%m"))
    archive_path = os.path.join(monthly_path, "archive/", last_subfolder)
    current_path = os.path.join(monthly_path, last_subfolder)
    if os.path.exists(monthly_path): # There are already reports for the current month
        print("There is a monthly path already")
        if os.path.exists(current_path):
            if not os.path.exists(archive_path):
                print("Creating the archival folder")
                os.makedirs(archive_path, exist_ok=True)

            number_of_archives = len(os.listdir(archive_path))
            # Move current wlcg to original/X/
            print("Archiving folder" )
            shutil.move(current_path, os.path.join(archive_path,str(number_of_archives)))

    print("Creating current report path")
    os.makedirs(current_path, exist_ok=True)
    working_path = os.getcwd()
    files_to_move = [f for f in os.listdir(working_path) if re.search("WLCG.*(\.pdf|\.json)",f)]
    print("Moving files to the final destination")
    for report_file in files_to_move:
        shutil.move(os.path.join(working_path, report_file),
                    os.path.join(current_path, report_file))


@click.command()
@click.option("--es-user", required=True, default="***", help='ElasticSearch username')
@click.option("--es-password", required=True, default="***", help='ElasticSearch password')
@click.option("--es-max-buckets", required=False, default=10000, help='ES maximum buckets per aggregation allowed')
@click.option("--idb-user", required=False, default="***", help='InfluxDB username')
@click.option("--idb-password", required=False, default="***", help='InfluxDB password')
@click.option("--past-months", required=False, default=1, help='Number of months to compare in the past')
@click.option("--output-path", required=False, default='/eos/project-m/monitoring/www/sitemonitoring/reports/')
@click.option("--comparison", is_flag=True, help='Run the tool in comparison mode')
@click.option("--original", is_flag=True, help='Do the comparison against reports without recomputation')
@click.option("--final", is_flag=True, help='Produce a "final" report, default is a "draft"')
def main(es_user, es_password, es_max_buckets, idb_user, idb_password, past_months, output_path, comparison, original, final):
    if comparison:
        run_comparison(es_user, es_password, es_max_buckets, idb_user, idb_password, past_months, original)
    else:
        run_report_generation(es_user, es_password, es_max_buckets, output_path, past_months, final)


if __name__ == '__main__':
    main()
